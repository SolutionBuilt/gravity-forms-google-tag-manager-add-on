<?php
/**
 * Plugin Name: Gravity Forms Google Tag Manager Add-On
 * Description: Gravity Forms add-on to capture form submissions and data with Google Tag Manager.
 * Version: 1.3
 * Update URI: https://bitbucket.org/SolutionBuilt/wordpress-plugin-updates/raw/master/info.json
 * Author: SolutionBuilt
 * Author URI: https://www.solutionbuilt.com
 * License: GPL-2.0+
 */


require_once plugin_dir_path( __FILE__ ) . 'includes/class-gfforms-gtm.php';

// Initialize
GFForms_GTM::get_instance();
