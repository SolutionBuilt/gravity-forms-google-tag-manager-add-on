<?php

class GFForms_GTM {

	/**
	 * Holds the class instance.
	 *
	 * @since 1.2
	 * @access private
	 */
	private static $instance = null;

	/**
	 * The filename for gravity forms plugin.
	 *
	 * @since 1.2
	 * @access private
	 */
	private static $gravityforms_plugin = 'gravityforms/gravityforms.php';


	/**
	 * Retrieve a class instance.
	 *
	 * @since 1.2
	 */
	public static function get_instance() {
		if ( null === self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}


	/**
	 * Class constructor.
	 *
	 * @since 1.2
	 */
	private function __construct() {
		// Trigger dataLayer event after successful form submission.
		add_filter( 'gform_confirmation', array( $this, 'on_form_submission' ), 10, 4 );
		add_action( 'wp_head', array( $this, 'process_submitted_form' ), 9 ); // Priority of 9 to ensure this appears above the GTM container for best accuracy

		// Require Gravity Forms plugin to be active.
		register_activation_hook( __FILE__, array( $this, 'on_self_activated' ) );
		add_action( 'admin_init', array( $this, 'require_gravityforms' ) );
		add_action( 'deactivated_plugin', array( $this, 'on_gravityforms_deactivated' ), 10, 2 );

		// Check for plugin update.
		add_filter( 'update_plugins_bitbucket.org', array( $this, 'check_for_update' ), 10, 4 );
	}


	/**
	 * Check for available updates for the plugin.
	 *
	 * @since 1.0
	 *
	 * @param array|false $update The plugin update data with the latest details. Default false.
	 * @param array       $plugin_data The plugin headers.
	 * @param string      $plugin_file The plugin filename.
	 * @param array       $locales Installed locales to look for.
	 *
	 * @return string
	 */
	public function check_for_update( $update, $plugin_data, $plugin_file, $locales ) {
		static $raw_response = false; // Prevent duplicate requests

		if (
			plugin_basename( __FILE__ ) !== $plugin_file ||
			empty( $plugin_data['UpdateURI'] ) ||
			! empty( $update )
		) {
			return $update;
		}

		if ( false === $raw_response ) {
			$raw_response = wp_remote_get(
				$plugin_data['UpdateURI'],
				array(
					'timeout' => 10,
					'headers' => array(
						'Accept' => 'application/json',
					),
				)
			);
		}

		$response = json_decode( wp_remote_retrieve_body( $raw_response ), true );

		// error_log( print_r( $response, false ), 3, __DIR__ .'/log.txt' );

		if ( ! empty( $response ) && ! empty( $response[ $plugin_file ] ) ) {
			return $response[ $plugin_file ];
		}

		return $update;
	}


	/**
	 * Add a form conversion event for a form submission.
	 *
	 * @since 1.0
	 *
	 * @param string|array $confirmation The confirmation message/array to be filtered.
	 * @param GFForm       $form The current form.
	 * @param GFEntry      $entry The current entry.
	 * @param bool         $is_ajax Specifies if this form is configured to be submitted via AJAX.
	 *
	 * @return string|array
	 */
	public function on_form_submission( $confirmation, $form, $entry, $is_ajax ) {
		// If form is AJAX-enabled and returns a confirmation message, then add dataLayer event to confirmation
		if ( $is_ajax && is_string( $confirmation ) ) {
			$confirmation .= wp_get_inline_script_tag( $this->push_data_layer_event( $form ) );

		// Trigger dataLayer event after page refresh or redirect
		} else {
			set_transient( 'gravityforms_gtm_submitted_form', $form );
		}

		return $confirmation;
	}


	/**
	 * Automatically deactivate if Gravity Forms plugin is deactivated
	 *
	 * @since 1.0
	 *
	 * @param string $plugin Path to the plugin file relative to the plugins directory.
	 * @param bool   $network_deactivation Whether the plugin is deactivated for all sites in the network or just the current site. Multisite only. Default false.
	 *
	 * @return void
	 */
	public function on_gravityforms_deactivated( $plugin, $network_deactivation ) {
		if ( $plugin === self::$gravityforms_plugin ) {
			add_action(
				'update_option_active_plugins',
				function() {
					deactivate_plugins( plugin_basename( __FILE__ ) );
				}
			);
		}
	}


	/**
	 * Run a routine after this plugin has been activated.
	 *
	 * @since 1.0
	 *
	 * @return void
	 */
	public function on_self_activated() {
		if ( current_user_can( 'activate_plugins' ) && file_exists( WP_PLUGIN_DIR . '/' . self::$gravityforms_plugin ) && ! is_plugin_active( self::$gravityforms_plugin ) ) {
			activate_plugin( self::$gravityforms_plugin );
		}
	}


	/**
	 * Add a form conversion event for a previously submitted form submission.
	 *
	 * IMPORTANT: Page caching breaks this functionality. For best results, exclude form "thank-you" pages from being cached.
	 *
	 * @since 1.0
	 *
	 * @return void
	 */
	public function process_submitted_form() {
		$key  = 'gravityforms_gtm_submitted_form';
		$form = get_transient( $key );

		if ( false !== $form ) {
			wp_print_inline_script_tag( $this->push_data_layer_event( $form ) );

			delete_transient( $key );
		}
	}

	/**
	 * Push event into the data layer.
	 *
	 * @since 1.0
	 *
	 * @param array $form The Form Object.
	 *
	 * @return void
	 */
	public function push_data_layer_event( $form ) {
		return sprintf(
			"
				window.dataLayer = window.dataLayer || [];

				window.dataLayer.push({
					'event': 'gf_submission',
					'formId': %d,
					'formLabel': '%s',
				});
			",
			$form['id'],
			$form['title']
		);
	}


	/**
	 * Prevent plugin activation if Gravity Forms is not installed or is blocked by WP Local Toolbox
	 *
	 * @since 1.0
	 *
	 * @return void
	 */
	public function require_gravityforms() {
		// Note: Use register_activation_hook to activate Gravity Forms plugin if installed.
		//	   Attempting to activate here will work, but won't show the plugin as active until the next page load.

		if ( current_user_can( 'activate_plugins' ) && ! is_plugin_active( self::$gravityforms_plugin ) ) {
			deactivate_plugins( plugin_basename( __FILE__ ) );

			add_action(
				'admin_notices',
				function() {
					unset( $_GET['activate'] );

					echo '<div class="notice notice-error is-dismissible"><p>Plugin cannot be activated. Please install and activate Gravity Forms plugin first.</p></div>';
				}
			);
		}
	}
}
